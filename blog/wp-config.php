<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'niels' );

/** Database password */
define( 'DB_PASSWORD', 'P!ppa/2001' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define( 'FS_METHOD', 'direct' );


/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D:<`{<Js8,Sq6M7,<b(>/!ck%T%`|NYoZ{o+_rw[lhSV9+UCCRSeWrdA E-<n|h=');
define('SECURE_AUTH_KEY',  'EGnw?I6_ReV/jj!^rJlpH)kw/O%h.j@]MxeSN&Di+PJ:dnO14v-Td4S*83Fo+xbF');
define('LOGGED_IN_KEY',    '=~4<EmJI^[uU]{5H wb%I?3 ^B`wU+gbc;oh`(Mjq;S(<~~m}8sZU$DTY1uhx/D<');
define('NONCE_KEY',        'q<;0%e2`n896-+T lG|{*?%+hBa<y4jWax-k;]tEQr$v)tVMCJoQ^o+tRhv&zQ.I');
define('AUTH_SALT',        ';nts1*,D< iD>?eDydpY%-=LIZtHB+O]E]W]1;#Pw<$EKf4e|]n^(8uW`$C-]J U');
define('SECURE_AUTH_SALT', '76[).T@4!MO+P=hd+>5A*>)G2wq1a8GUm2i`b>J.xC?++f+^[ed0N]Ab:EVex0Kw');
define('LOGGED_IN_SALT',   'PZluDz^B@ SXYl9>?+-Q adi 7gsZ!N:HXrU%/GawB$r./xI,{zF/k[W+EA+)uVk');
define('NONCE_SALT',       '8)>E$5qzJ+}[E3tLQ|D4~xU$6)5=kq1q7(1VJfs?O3X;+G,l-u.q/]HfUI4q&w@L');

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */
define( 'WP_HOME', 'http://localhost/blog' );
define( 'WP_SITEURL', 'http://localhost/blog' );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
